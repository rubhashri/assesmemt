__author__ = 'Rubha'

import numpy as np
import pandas as pd
from sklearn.feature_extraction import DictVectorizer as DV
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.metrics import roc_auc_score as AUC
from sklearn import preprocessing
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn.decomposition import PCA
from sklearn.feature_selection import SelectKBest
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.model_selection import train_test_split
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.gaussian_process.kernels import RBF
from sklearn.externals import joblib
from tkinter import Tk
from tkinter import filedialog
import xlrd
import csv
import numpy as np

Tk().withdraw()
Filename = filedialog.askopenfilename(filetypes = (("Excel Files", "*.csv"), ("All files", "*")))
print(Filename)

if __name__ == '__main__':

    data = pd.read_csv( Filename, low_memory=False )

    #Replacing non-integer
    data['diag_1'] = pd.to_numeric(data.diag_1.astype(str).str.replace(',',''), errors='coerce').fillna(0).astype(int)
    data['diag_2'] = pd.to_numeric(data.diag_1.astype(str).str.replace(',',''), errors='coerce').fillna(0).astype(int)
    data['diag_3'] = pd.to_numeric(data.diag_1.astype(str).str.replace(',',''), errors='coerce').fillna(0).astype(int)
    data['num_medications'] = pd.to_numeric(data.diag_1.astype(str).str.replace(',',''), errors='coerce').fillna(0).astype(int)
    data['num_lab_procedures'] = pd.to_numeric(data.diag_1.astype(str).str.replace(',',''), errors='coerce').fillna(0).astype(int)

    data.diag_1[ data.diag_1 == 0 ] = data.diag_1.mean()
    data.diag_2[ data.diag_2 == 0 ] = data.diag_2.mean()
    data.diag_3[ data.diag_3 == 0 ] = data.diag_3.mean()
    data.num_medications[ data.num_medications == 0 ] = data.num_medications.mean()
    data.num_lab_procedures[ data.num_lab_procedures == 0 ] = data.num_lab_procedures.mean()

    numeric_cols = ['num_lab_procedures', 'num_medications','diag_1', 'diag_2' ,'diag_3']

    X = data[ numeric_cols ].as_matrix()
    #print(X)

    max_train = np.amax( X, 0 )
    X = X / max_train


    y = data.readmitted
    #print(y)

    #Dictionay
    Dict_X = data.drop( numeric_cols + [ 'encounter_id', 'patient_nbr', 'readmitted'], axis = 1 )
    #print(cat_X)

    Dict_X.dropna(axis=1, how='all')
    Dict_X.fillna( ' ', inplace = True )
    Dict_X_dict = Dict_X.to_dict( orient = 'records' )


    vectorizer = DV( sparse = False )
    vec_X = vectorizer.fit_transform( Dict_X_dict )

    x = np.hstack(( X, vec_X ))
    #print(np.any(np.isnan(x)))
    print(x.dtype, y.dtype)
    #print(x)

    #Feature Selection
    pca = PCA(n_components=8)
    selection = SelectKBest(chi2)
    combined_features = FeatureUnion([("pca", pca), ("univ_select", selection)])
    X_features = combined_features.fit(x, y).transform(x)

    #Classifiers
    gamma = 1.31e-5
    shrinking = True
    probability = True
    verbose = True
    C = 1.0
    kernel = 1.0 * RBF([1.0, 1.0])  # for GPC
    clf  = svm.SVC(kernel="linear")
    #clf =  SVC(kernel='linear', C=C, probability=True,random_state=0)
    #clf = LogisticRegression(C=C, penalty='l2')
    #clf =GaussianNB()
    #clf = LogisticRegression(C=C, penalty='l1')
    #clf =  SVC( C = C, gamma = gamma, shrinking = shrinking, probability = probability, verbose = verbose )
    #clf = LogisticRegression( C=C, solver='lbfgs', multi_class='multinomial')

    X_train, X_test, y_train, y_test = train_test_split(X_features, y, test_size=0.3, random_state=0)
    Classifier = clf.fit(X_train, y_train)
    Acc = Classifier.score(X_test, y_test)
    print(Acc)

    #Model
    clf.fit(X_features, y)
    joblib.dump(clf, 'Diabetic.pkl')





